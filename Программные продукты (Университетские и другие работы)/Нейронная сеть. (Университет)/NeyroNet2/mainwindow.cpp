#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVector>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <qmath.h>
#define N 50
#define K 2
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){

    ui->setupUi(this); 
}



QVector <QVector <double>> vyborkaVhod(100, QVector <double>(3));
QVector <QVector <double>> vyborkaVyhod(100, QVector <double>(2));
QVector <QVector <double>> w(3, QVector <double>(2));
QVector <double> sNeyrona(2), VyhodRasschit(2), pogr(2), w0(2) ,oshibka(0), epoha(0), mSpeed(0);
double a =0.4, e =0.08, oBoshibka1 =0.0, oBoshibka2 =0.0, oBoshibka3 =0.0;
double speed = 0.9;
double functionUidrouHoffa(int indexVh, int indexVyh);
int ekol;



QVector <QVector <double>> wT1(3, QVector <double>(2)) ,wT2(3, QVector <double>(2));
QVector <double> w0T1(2), w0T2(2);

double functionNormVhod(double x, double x1, double x2, double x3){

    double xmin, xmax, xcen;
    if((x1 <x2) &&(x1 <x3)){
        xmin =x1;
    }else{
        if((x2 <x3) &&(x2 <x1)){
            xmin =x2;
        }else{
            xmin =x3;
        }
    }

    if((x1 >x2) &&(x1 >x3)){
        xmax =x1;
    }else{
        if((x2 >x3) &&(x2 >x1)){
            xmax =x2;
        }else{
            xmax =x3;
        }
    }
    xcen =(xmin +xmax) /2;

    return (qExp(a *(x -xcen)) -1) /(qExp(a *(x -xcen)) +1);
}


double functionNormVyhod(double x, double x1){

    double xmin, xmax, xcen;
    if(x <x1){
        xmin =x;
    }else{
            xmin =x1;
        }

    if(x >x1){
        xmax =x;
    }else{
            xmax =x1;
        }
    xcen =(xmin +xmax) /2;

    return (qExp(a *(x -xcen)) -1) /(qExp(a *(x -xcen)) +1);
}

void functionGO(){

    //Normalizuem
    for(int i =0; i <100; i++){
        for (int j =0; j <3; j++){
            vyborkaVhod[i][j] = rand() %20 -10;
            vyborkaVhod[i][j] =functionNormVhod(vyborkaVhod[i][j], vyborkaVhod[i][0], vyborkaVhod[i][1], vyborkaVhod[i][2]);
        }
            vyborkaVyhod[i][0] =functionNormVyhod(vyborkaVyhod[i][0] ,vyborkaVyhod[i][1]);
            vyborkaVyhod[i][1] =functionNormVyhod(vyborkaVyhod[i][0] ,vyborkaVyhod[i][1]);
    }

    ekol =0;


    //Ustanovka  vesovyh koefitsientov

    for(int i =0; i <3; i++){
        for(int j =0; j <2; j++){
            w[i][j] = (rand() %3 -3) /10.0;
            w0[j] =(rand() %3 -3) /10.0;
        }
    }


}



double functionUidrouHoffa(int indexVh, int indexVyh){

        sNeyrona[0] =w0[0] +w[0][0] *vyborkaVhod[indexVh][0] +w[1][0] *vyborkaVhod[indexVh][1] +w[2][0] *vyborkaVhod[indexVh][2];
        sNeyrona[1] =w0[1] +w[0][1] *vyborkaVhod[indexVh][0] +w[1][1] *vyborkaVhod[indexVh][1] +w[2][1] *vyborkaVhod[indexVh][2];

//        VyhodRasschit[0] = 1 /(qExp(-a *sNeyrona[0]) +1);
//        VyhodRasschit[1] = 1 /(qExp(-a *sNeyrona[1]) +1);

        VyhodRasschit[0] = (qExp(a *sNeyrona[0]) -1) /(qExp(a *sNeyrona[0] +1));
        VyhodRasschit[1] = (qExp(a *sNeyrona[1]) -1) /(qExp(a *sNeyrona[1] +1));

        pogr[0] =vyborkaVyhod[indexVyh][0] -VyhodRasschit[0];
        pogr[1] =vyborkaVyhod[indexVyh][1] -VyhodRasschit[1];



        for( int i =0; i <3; i++){
            for ( int j =0; j <2; j++){
                w[i][j] =w[i][j] +speed *pogr[j] *vyborkaVhod[indexVh][i];
//                cout << w[i][j] <<endl;
            }
        }
        for ( int j =0; j <2; j++){
            w0[j] =w0[j] +speed *pogr[j];
        }

        return (pow(pogr[0] ,2) +pow(pogr[1] ,2)) /K;
}




void MainWindow::on_pushButton_clicked(){
    functionGO();


    while(speed >0){
        oBoshibka1 =0;
        while(1 >0){
            for(int i =0; i <50; i++){
                oBoshibka1 +=functionUidrouHoffa(i, i);
            }
            oBoshibka1 = sqrt(oBoshibka1 / N /K);

            ekol++;

            if(oBoshibka1 <e){
                break;
            }
            if(ekol > 10000){
                break;
            }
            oBoshibka1 =0;
        }
        mSpeed.push_back(speed);
        speed -= 0.15;
        oshibka.push_back(oBoshibka1);
        epoha.push_back(ekol);
    }
    for(int i =0; i <3; i++){
        for(int j =0; j <2; j++){
            wT1[i][j] = w[i][j];
            w0T1[j] = w0[j];
        }
    }


    cout << "Obshaya oshibka =" << oBoshibka1 <<endl;

    ui->label->setText("Зависимость продолжительности обучения от коэфициента скорости. e =" +QString::number(e));

    ui->label_2->setText("Общая ошибка =" +QString::number(oBoshibka1));

//    ui->widget->legend->setVisible(true);
//    ui->widget1->legend->setVisible(true);
    ui->widget->clearGraphs();//Если нужно, но очищаем все графики

    //Рисуем точки
    ui->widget->xAxis->setRange(0, epoha.size() +1);//Для оси Ox
    ui->widget->yAxis->setRange(0, 1);//Для оси Oy


    //Добавляем один график в widget
    ui->widget->addGraph();
    //Говорим, что отрисовать нужно график по нашим двум массивам x и y
    ui->widget->graph(0)->setData(epoha, mSpeed);
    ui->widget->graph(0)->setPen(QColor(0, 0, 255, 255));//задаем цвет точки
    ui->widget->graph(0)->setLineStyle(QCPGraph::lsLine);//убираем линии

    //Подписываем оси Ox и Oy
    ui->widget->xAxis->setLabel("Количество эпох");
    ui->widget->yAxis->setLabel("коэфициент скрорости");



    functionGO();
    cout <<epoha.size() << endl<<endl;
    speed =0.9;
    int j =0;
    oBoshibka2 =0;
    while(1 >0){
//            ekol++;
        speed = (epoha.size() -ekol +1) /epoha.size();
        for(int i =0; i <50; i++){
            oBoshibka2 +=functionUidrouHoffa(i, i);
        }
        oBoshibka2 = sqrt(oBoshibka2 / N /K);

        j++;
        if(oBoshibka2 <e){
            break;
        }
//        if( 6 <= j){
        if (5 <= j){
            break;
        }
        oBoshibka2 =0;
    }


    for(int i =0; i <3; i++){
        for(int j =0; j <2; j++){
            wT2[i][j] = w[i][j];
            w0T2[j] = w0[j];
        }
    }


    cout << "Obshaya oshibka =" << oBoshibka2 <<endl;



    ui->label_3->setText("Общая ошибка =" +QString::number(oBoshibka2));
//    ui->widget1->clearGraphs();//Если нужно, но очищаем все графики

    ui->label->setText("Зависимость продолжительности обучения от коэфициента скорости");
//    //Рисуем точки
//    ui->widget1->xAxis->setRange(5, epoha.size() *2);//Для оси Ox
//    ui->widget1->yAxis->setRange(0, 1);//Для оси Oy


    //Добавляем один график в widget
//    ui->widget1->addGraph();
//    //Говорим, что отрисовать нужно график по нашим двум массивам x и y
//    ui->widget1->graph(0)->setData(epoha, mSpeed);
//    ui->widget1->graph(0)->setPen(QColor(0, 255, 0, 255));//задаем цвет точки
//    ui->widget1->graph(0)->setLineStyle(QCPGraph::lsLine);//убираем линии

    //Подписываем оси Ox и Oy
//    ui->widget1->xAxis->setLabel("Количество эпох");
//    ui->widget1->yAxis->setLabel("коэфициент скрорости");


    ui->widget->graph(0)->setName("Постоянный коэфициент скорости");
//    ui->widget1->graph(0)->setName("Линейно убывающий коэфициент скорости");
    ui->widget->replot();
//    ui->widget1->replot();




    //Test GO
    if(oBoshibka1 <oBoshibka2){
        for(int i =0; i <3; i++){
            for(int j =0; j <2; j++){
                w[i][j] =wT1[i][j];
                w0[j] =w0T1[j];
            }
        }
    }
    for(int i =50; i <100; i++){
        oBoshibka3 +=functionUidrouHoffa(i, i);
    }
    oBoshibka3 = sqrt(oBoshibka3 / N /K);

    ui->label_4->setText("Ошибка при тестировании =" +QString::number(oBoshibka3));

}






MainWindow::~MainWindow(){
    delete ui;
}


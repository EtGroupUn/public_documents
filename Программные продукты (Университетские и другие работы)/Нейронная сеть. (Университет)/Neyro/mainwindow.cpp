#include "mainwindow.h"
#include "ui_mainwindow.h"


double yfun(double s, double a, double t);
QVector <double> x1(22),x2(22)
    ,w0(22), w1(22) ,w2(22)
    ,sx1(22) ,sx2(22)
    ,sw0(22) ,sw1(22) ,sw2(22)
    ,yx1(22) ,yx2(22)
    ,yw0(22) ,yw1(22) ,yw2(22)
    ,ya(22) ,yt(22)
    ,a(22)
    ,t(22);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    double fixX1 =0.2, fixX2 =0.25, fixW0 =0.4, fixW1 =0.5, fixW2 =0.7, ct =0.6, ca =20;


    x1[0] =x2[0] =0;
    w0[0] =w1[0] =w2[0] = -0.10;
    a[0] =-1;
    t[0] =-2;

    //Задаю начальные значения
    for(int i =0; i <21; i++){
        x1[i +1] =x1[i] +0.05;
        x2[i] =x1[i];
//        cout <<x1[i] << "\t" <<endl;
        w0[i +1] =w0[i] +0.05;
        w1[i] =w2[i] =w0[i];
//        cout <<w0[i] << "\t" <<endl;
        a[i +1] =a[i] +0.3;
        t[i +1] =t[i] +1.1;
//      cout <<t[i] << "\t" <<endl;
    }


    for (int i =0; i <21; i++){
        sx1[i] =x1[i] *fixW1 +(fixX2 *fixW2) +fixW0;
        sx2[i] =fixX1 *fixW1 +(x2[i] *fixW2) +fixW0;
        sw0[i] =fixX1 *fixW1 +(fixX2 *fixW2) +w0[i];
        sw1[i] =fixX1 *w1[i] +(fixX2 *fixW2) +fixW0;
        sw2[i] =fixX1 *fixW1 +(fixX2 *w2[i]) +fixW0;

        yx1[i] =yfun(sx1[i], ca, ct);
        yx2[i] =yfun(sx2[i], ca, ct);
        yw0[i] =yfun(sw0[i], ca, ct);
        yw1[i] =yfun(sw1[i], ca, ct);
        yw2[i] =yfun(sw2[i], ca, ct);

        ya[i] =yfun(sw2[i], a[i], ct);
        yt[i] =yfun(sw2[i], ca, t[i]);
    }

    ui->setupUi(this);
    //Ставим диапазон
    ui->widget->xAxis->setRange(0, 1);//Для оси Ox
    ui->widget->yAxis->setRange(0, 1);//Для оси Oy
    //Подписываем оси Ox и Oy
    ui->widget->xAxis->setLabel("x");
    ui->widget->yAxis->setLabel("y");

    ui->widget->legend->setVisible(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}


double yfun(double s, double a, double t){

    return 1/(qExp( -a *(s -t)) +1);

}


void MainWindow::on_pushButton_clicked(){

    ui->label->setText("Зависимость выходных данных от входных переменных");
    //Рисуем точки

    ui->widget->clearGraphs();//Если нужно, но очищаем все графики
    //Добавляем один график в widget
    ui->widget->addGraph();
    //Говорим, что отрисовать нужно график по нашим двум массивам x и y
    ui->widget->graph(0)->setData(x1 ,yx1);
    ui->widget->graph(0)->setPen(QColor(0, 0, 255, 255));//задаем цвет точки
    ui->widget->graph(0)->setLineStyle(QCPGraph::lsLine);//убираем линии
    ui->widget->addGraph();
    ui->widget->graph(1)->setData(x2 ,yx2);
    ui->widget->graph(1)->setPen(QColor(0, 255, 0, 255));//задаем цвет точки
    ui->widget->graph(1)->setLineStyle(QCPGraph::lsLine);//убираем линии

    //формируем вид точек
//    ui->widget->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 4));

    //Подписываем оси Ox и Oy
    ui->widget->xAxis->setLabel("x");
    ui->widget->yAxis->setLabel("y");

    //Установим область, которая будет показываться на графике
    ui->widget->xAxis->setRange(0, 1.5);//Для оси Ox
    ui->widget->yAxis->setRange(-0.05, 1);//Для оси Oy

    ui->widget->graph(1)->setName("yx1");
    ui->widget->graph(0)->setName("yx2");

    //И перерисуем график на нашем widget
    ui->widget->replot();
}



void MainWindow::on_pushButton_2_clicked(){
    ui->label->setText("Зависимость выходных данных от синаптических коэфициентов");
    //Рисуем точки

    ui->widget->clearGraphs();//Если нужно, но очищаем все графики
    //Добавляем один график в widget
    ui->widget->addGraph();
    //Говорим, что отрисовать нужно график по нашим двум массивам x и y
    ui->widget->graph(0)->setData(w0 ,yw0);
    ui->widget->graph(0)->setPen(QColor(0, 0, 255, 255));//задаем цвет точки
    ui->widget->graph(0)->setLineStyle(QCPGraph::lsLine);

    ui->widget->addGraph();
    ui->widget->graph(1)->setData(w1 ,yw1);
    ui->widget->graph(1)->setPen(QColor(255, 0, 0, 255));//задаем цвет точки
    ui->widget->graph(1)->setLineStyle(QCPGraph::lsLine);

    ui->widget->addGraph();
    ui->widget->graph(2)->setData(w2 ,yw2);
    ui->widget->graph(2)->setPen(QColor(0, 255, 0, 255));//задаем цвет точки
    ui->widget->graph(2)->setLineStyle(QCPGraph::lsLine);


    ui->widget->graph(0)->setName("yw0");
    ui->widget->graph(1)->setName("yw1");
    ui->widget->graph(2)->setName("yw2");

    //Установим область, которая будет показываться на графике
    ui->widget->xAxis->setRange(0, 1.5);//Для оси Ox
    ui->widget->yAxis->setRange(-0.05, 1);//Для оси Oy

    //Подписываем оси Ox и Oy
    ui->widget->xAxis->setLabel("w");
    ui->widget->yAxis->setLabel("y");
    ui->widget->replot();
}

void MainWindow::on_pushButton_3_clicked(){

    ui->label->setText("Зависимость выходных данных от изменения параметра < а >");
    //Рисуем точки

    ui->widget->clearGraphs();//Если нужно, но очищаем все графики
    //Добавляем один график в widget
    ui->widget->addGraph();
    //Говорим, что отрисовать нужно график по нашим двум массивам x и y
    ui->widget->graph(0)->setData(a ,ya);
    ui->widget->graph(0)->setPen(QColor(0, 0, 255, 255));//задаем цвет точки
    ui->widget->graph(0)->setLineStyle(QCPGraph::lsLine);
    ui->widget->graph(0)->setName("ya");
    //Ставим диапазон
    ui->widget->xAxis->setRange(-1, 2);//Для оси Ox
    ui->widget->yAxis->setRange(-0.05, 1);//Для оси Oy
    //Подписываем оси Ox и Oy
    ui->widget->xAxis->setLabel("a");
    ui->widget->yAxis->setLabel("y");

    ui->widget->replot();
}

void MainWindow::on_pushButton_4_clicked(){

    ui->label->setText("Зависимость выходных данных от параметра < t >");
     //Рисуем точки

     ui->widget->clearGraphs();//Если нужно, но очищаем все графики
     //Добавляем один график в widget
     ui->widget->addGraph();
     //Говорим, что отрисовать нужно график по нашим двум массивам x и y
     ui->widget->graph(0)->setData(t ,yt);
     ui->widget->graph(0)->setPen(QColor(0, 0, 255, 255));//задаем цвет точки
     ui->widget->graph(0)->setLineStyle(QCPGraph::lsLine);

     ui->widget->graph(0)->setName("yt");
     //Ставим диапазон
     ui->widget->xAxis->setRange(-2, 20);//Для оси Ox
     ui->widget->yAxis->setRange(-0.05, 1);//Для оси Oy
     //Подписываем оси Ox и Oy
     ui->widget->xAxis->setLabel("t");
     ui->widget->yAxis->setLabel("y");

     ui->widget->replot();

}

import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQml 2.2
import QtQuick.Window 2.0
import QtCharts 2.1


Rectangle {
    id: rect

    Rectangle {
        id: rectangle1
        width: 228
        height: 56
        color: "#fa2020"
        anchors.verticalCenterOffset: height /2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Text {
            id: text1
            width: 214
            height: 48
            color: "#ffffff"
            text: qsTr("Метод Нелдера-Мида")
            font.bold: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 15
        }

        MouseArea {
            id: mouseArea1
            anchors.fill: parent
            onClicked: window.functionGo()
        }
    }

    Rectangle {
        id: rectangle2
        x: 257
        y: 302
        width: 126
        height: 37
        color: "#a04ff0"

        Text {
            id: text2
            color: "#ffffff"
            text: qsTr("Метод штрафов")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 12
        }

        MouseArea {
            id: mouseArea2
            anchors.fill: parent
            onClicked: window.functionShtraf()
        }
    }

}

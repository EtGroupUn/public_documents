#-------------------------------------------------
#
# Project created by QtCreator 2016-12-20T19:36:53
#
#-------------------------------------------------

QT       += core gui quick qml charts
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjectPrinOpR
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    +=

DISTFILES += \
    main.qml

RESOURCES += \
    res.qrc

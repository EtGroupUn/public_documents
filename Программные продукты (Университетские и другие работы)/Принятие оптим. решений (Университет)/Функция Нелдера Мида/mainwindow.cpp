#include "mainwindow.h"
#include "mainwindow.ui"
#include <QCoreApplication>
#include <QVector>


#define N 2

double functionNelderaMida();
double function(double ,double);
void functionProverka();
void functionShtraf();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent){


    ui = new QQuickView;
    //Включаем наш QML
    //подготавливаем ui
    ui->setSource(QUrl("qrc:/main.qml"));
    ui->setResizeMode(QQuickView::SizeRootObjectToView);
    //    //Находим корневой элемент (т.е основной)
    Root = ui->rootObject();
    //    //Соединяем C++ и QML, делая видимыми функции C++ через элемент window
    ui->rootContext()->setContextProperty("window" ,this);

    ui->show();
}



MainWindow::~MainWindow(){
    delete ui;
}




// Массив для задания координатов точек
QVector <QVector <double>> fun(7, QVector <double>(3));
double alpha =1, beta =0.5, gama =2;
//double e =0.02;
double e =0.05;

int kol =0;
bool stop =false;
//индексы для массивов чтоб память сэкономить
int minI, maxI, srI;

double function(double a, double b){

    double f;

//    f = 4 *pow((a -5),2) +pow((b -6),2);
    f = 2 *pow(a ,2) +a *b +pow(b ,2);

    return f;
}

void functionProverka(){

    //проверка приближения если все ок, то берем лучшую точку
    double fi;

    fi = sqrt ((pow ((fun[0][2] -fun[3][2]) ,2) +pow( (fun[1][2] -fun[3][2]) ,2)
            +pow( (fun[2][2] -fun[3][2]) ,2)) /(N+1));
    std ::cout <<"fi =" <<std ::setprecision (8) <<fi <<std ::endl;

    if(fi <=e){

        std ::cout << "raschet vypolnen =" <<fun[minI][2] <<std ::endl;
        std ::cout << "x =" <<fun[minI][0] <<" x2 =" <<fun[minI][1] <<std ::endl;
        stop =true;

    }
}

void MainWindow::functionGo(){

    //задаем начальные данные для точек
    //x1
    fun[0][0] =0.5;
    fun[0][1] =1;
    //x2
    fun[1][0] =0;
    fun[1][1] =0.5;
    //x3
    fun[2][0] =1;
    fun[2][1] =0.5;


    shag2:
    //посчитаем значения в точках
    for (int i =0; i <3; i++){

        fun[i][2] =function(fun[i][0], fun[i][1]);
        std::cout <<fun[i][2] <<std::endl;

    }


    //найдем минимум и максимум
    minI =0;
    maxI =0;
    srI =0;


    for (int i =0; i <3; i++){
        if(fun[i][2] <fun[minI][2])
            minI =i;
    }
    for (int i =0; i <3; i++){
        if(fun[i][2] >fun[maxI][2])
            maxI =i;
            }
    for (int i =0; i <3; i++){
        if((fun[i][2] <fun[maxI][2]) && (fun[i][2] >fun[minI][2]))
            srI =i;
    }


    std ::cout << "Min =" <<fun[minI][2] <<std ::endl;
    std ::cout << "Max =" <<fun[maxI][2] <<std ::endl;
    std ::cout << "Sr =" <<fun[srI][2] <<std ::endl;
//    if(kol ==2){
//        std::cout << fun[6][2] <<std::endl;
//        exit(0);
//    }

    //найдем центр тяжести он же х +2
    fun[3][0] =(fun[0][0] +fun[1][0] +fun[2][0] -fun[maxI][0]) /N;
    fun[3][1] =(fun[0][1] +fun[1][1] +fun[2][1] -fun[maxI][1]) /N;
    fun[3][2] =function(fun[3][0], fun[3][1]);
    std::cout << "Centr tyajesti ="<<fun[3][2] <<std::endl;


    //проверка приближения
    functionProverka();
    if(stop == true){
        return;
    }
    //fun[2][2] - x +2
    //fun[3][2] - x +3
    //fun[4][2] - x +4

    //будем отражать
    std::cout << "Otrajaem " <<std::endl;

    fun[4][0] =fun[3][0] +alpha *(fun[3][0] -fun[maxI][0]);
    fun[4][1] =fun[3][1] +alpha *(fun[3][1] -fun[maxI][1]);
    fun[4][2] =function(fun[4][0], fun[4][1]);


    if(fun[4][2] <=fun[minI][2]){
        std::cout << "Rastyagivaem" <<std::endl;
        fun[5][0] =fun[3][0] +gama *(fun[4][0] -fun[3][0]);
        fun[5][1] =fun[3][1] +gama *(fun[4][1] -fun[3][1]);
        fun[5][2] =function(fun[5][0], fun[5][1]);

        if(fun[5][2] <fun[minI][2]){

            fun[maxI][0] =fun[5][0];
            fun[maxI][1] =fun[5][1];
            kol++;
            goto shag2;
        }
        if(fun[5][2] >=fun[minI][2]){
            fun[maxI][0] =fun[4][0];
            fun[maxI][1] =fun[4][1];
            fun[maxI][2] =function(fun[maxI][0], fun[maxI][1]);
            kol++;
            goto shag2;
        }
    }
    //
    //
    //
    //
    //ШАГ 6 вариант Б
    if( (fun[srI][2] <fun[4][2]) && (fun[4][2] <=fun[maxI][2]) ){
        std::cout << "Sjimaem " <<std::endl;
        fun[6][0] =fun[3][0] +beta *(fun[maxI][0] -fun[3][0]);
        fun[6][1] =fun[3][1] +beta *(fun[maxI][1] -fun[3][1]);
        fun[6][2] =function(fun[6][0], fun[6][1]);
        fun[maxI][2] =fun[6][2];
        fun[maxI][0] =fun[6][0];
        fun[maxI][1] =fun[6][1];
        kol++;

        goto shag2;
    }


    //Шаг 6 вариант В
    if( (fun[minI][2] <fun[4][2]) && (fun[4][2] <=fun[srI][2])){

        fun[maxI][0] =fun[4][0];
        fun[maxI][1] =fun[4][1];
        fun[maxI][2] =function(fun[maxI][0], fun[maxI][1]);

        kol++;

        goto shag2;
    }



    //Шаг 6 вариант Г


    if( fun[4][2] >fun[maxI][2]){
        std::cout <<"redukciya" <<std::endl;
        for(int i =0; i <3; i++){
            if (minI != i){
                fun[i][0] =fun[minI][0] +((fun[i][0] -fun[minI][0]) /2);
                fun[i][1] =fun[minI][1] +((fun[i][1] -fun[minI][1]) /2);
            }
        }
        kol++;
        goto shag2;
    }
    std ::cout << "shag 6 vypolnen";
}



void MainWindow::functionShtraf(){
    double x1 =1 ,x2 =1;
    double F ,r =1;

    double h;

    std ::cout <<"r" <<"  " <<"x1" <<" " <<"function(x1 ,x2)" <<"  " <<"F" <<"  " <<"p" <<std ::endl;

    for (int i =0; i <10; i++){
        h =x1 +x2;
        F =function(x1 ,x2) +r *h*h;
        std ::cout <<r <<"  " <<x1 <<" " <<function(x1 ,x2) <<"  " <<F <<"  " <<h*h <<std ::endl;
        r *= 10;
    }


























}

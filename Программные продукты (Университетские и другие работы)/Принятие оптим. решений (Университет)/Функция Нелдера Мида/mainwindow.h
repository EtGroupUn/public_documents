#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <iomanip>
#include <QMainWindow>
#include <QQuickView>
#include <QtQuick>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Q_INVOKABLE void functionGo();
    Q_INVOKABLE void functionShtraf();

private:
    QQuickView *ui;
    QObject *Root; //корневой элемент QML-модели
};

#endif // MAINWINDOW_H

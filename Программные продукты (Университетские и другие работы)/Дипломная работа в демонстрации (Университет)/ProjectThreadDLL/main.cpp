#include <cstdlib>
#include <iostream>
#include <windows.h>
#include <time.h>
#include <stdlib.h>
#include <ctime>
#include <omp.h>
#include <thread>

int N;
using namespace std;
typedef void (*DLLlib)(int *, int, int, int);
int compare(const void * x1, const void * x2);
int raskidka(int mas[],  int Count);
void OMPqSort(int mas[],  int Count);
void functionThread1(int mas[] ,int srEl);
void functionThread2(int mas[] ,int srEl);

int main(){

    unsigned int start_time;
    unsigned int end_time ;
    unsigned int search_time;

    cout <<"Mode of implementation\n\t0)AssemblerDll\n\t1)OMP\n\t2)Standart qSort(C++)\n\t3)Win32Threads\nEnter you mode"<<endl;

    int mode;
    cin >> mode;
    if((mode >3) || (mode <0)){
            cout << "Input is not correction"<<endl;
            return 0;
    }
    cout <<"Enter number of elements in random massive (0 is defaul = 10 000 000)" << endl;
    cin >>N;
    if(N ==0){
        N = 10000000;
    }

cout <<"Enter number loops:\n" <<endl;
int j;
cin >>j;

srand(time(0));
clock_t timer;
int *mas = new int [N];

for (int i =0; i <j; i++){



    for (int i= 0; i< N; i++){
           mas [i]= rand()%100;
//           cout << mas[i] << " ";
       }
       cout <<endl;



    switch (mode){
        case 0:{

        int threadNum;
        if(i ==0){
            cout <<"Enter number threads" << endl;
            cin >>threadNum;
            if((threadNum >3)||(threadNum <0)){
                cout << "Incorrect number of thread Dll"<< endl;
                return 0;
            }
        }


            //      Блок работы с Ассемблерной Длл
           HINSTANCE hMyDll;
           if ((hMyDll =LoadLibrary(L"PSort.dll")) == NULL){

              cout <<"dll is not founded" <<endl;
//              return 0;
           }else{
           cout << "dll is loading"<< endl;
           }
          DLLlib dllFun;
          dllFun = (DLLlib)GetProcAddress(hMyDll, "PSort");
          start_time =  clock();//Запуск таймера
          dllFun(mas, N, threadNum, 1); //(массив, кол-во эл, кол-во витков, номер способа(1,2))
          end_time = clock();
          search_time = end_time - start_time;//Рассчёт затраченного 											//времени

//          for(int i = 0; i < N; ++i){
//            cout << mas[i] << " ";
//          }
//          cout <<endl;

          cout << search_time << endl;

        FreeLibrary(hMyDll);

        break;
            //    Закончился блок работы с Ассемблерной Длл
        }
        case 1:{
                // Блок работы OpenMP

            start_time =  clock();//Запуск таймера
               OMPqSort(mas ,N);

            end_time = clock();
            search_time = end_time - start_time;//Рассчёт затраченного
            cout << "Waste Time =" <<search_time << endl;

//            //Вывод массива
//                    for(int i = 0; i < N -1; i++){
//                        cout << mas[i] << " ";
//                    }
//                    cout <<endl;


                // Закончился блок работы OpenMP
            break;
        }
        case 2:{
            //Блок работы стандартной библиотеки

            start_time =  clock();//Запуск таймера

            qsort(mas, N, sizeof(int), compare);      // сортируем массив чисел

            end_time = clock();
            search_time = end_time - start_time;//Рассчёт затраченного
            cout << "Waste Time =" <<search_time << endl;
            //Закончился блок работы стандартной библиотеки
            break;
        }
        case 3:{
            int thEl =raskidka(mas ,N);

            std::thread t1(functionThread1 ,mas ,thEl); //Создание потока1 (функция ,аргумент ,аргумент)
            std::thread t2(functionThread2 ,mas ,thEl); //Создание потока1 (функция ,аргумент ,аргумент)

            start_time =  clock();//Запуск таймера
            t1.join();
            t2.join();
            end_time = clock();
            search_time = end_time - start_time;//Рассчёт затраченного
            cout << "Waste Time =" <<search_time << endl;

//            //Вывод массива
//            for(int i = 0; i < N -1; i++){
//                 cout << mas[i] << " ";
//            }
//            cout <<endl;

        }
    }



};

    system("PAUSE");
//    delete[] mas;
}

void functionThread1(int mas[] ,int srEl){
    qsort(mas, srEl -1, sizeof(int), compare);

}
void functionThread2(int mas[] ,int srEl){
     qsort(mas +srEl, N -srEl, sizeof(int), compare);
}




int compare(const void * x1, const void * x2)   // функция сравнения элементов массива
{
  return ( *(int*)x1 - *(int*)x2 );              // если результат вычитания равен 0, то числа равны, < 0: x1 < x2; > 0: x1 > x2
}


//Быстрая сортировка средствами ОМП

void OMPqSort (int mas[],  int Count){


int srEl =raskidka(mas ,Count);

        #pragma omp parallel sections
        {
        #pragma omp section
            qsort(mas, srEl -1, sizeof(int), compare);      // сортируем массив чисел
         #pragma omp section
            qsort(mas +srEl, N -srEl, sizeof(int), compare);      // сортируем массив чисел
        }

}





//Раскидываю элементы массива по участкам памяти

int raskidka(int mas[],  int Count){
    int max =mas[0] ,min =mas[0];
    for (int i =0; i <Count; i++){
        if (mas[i] >max){
            max =mas[i];
        }
        if (mas[i] <min){
            min =mas[i];
        }
    }

//    cout << "max elem = " <<max <<endl <<"min elem = " <<min <<endl;

    int sr =max /2;
    int temp;
    int i =0, j =Count -1;

//    cout << "sr = " <<sr <<endl;

    while(i <j){
        if(mas[i] >sr){
            while(j >i){
                if(mas[j] <sr){
                    temp =mas[i];
                    mas[i] =mas[j];
                    mas[j] =temp;
                    break;
                }
                j--;
            }
        }
        i++;
    }

    return i;
}




